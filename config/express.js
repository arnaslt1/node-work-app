import express from 'express';
import routes from '../server/routes';
import bodyParser from 'body-parser';
import expressValidation from 'express-validation';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// mounts all routes to api path

app.use((err, req, res, next) => {
  if (err instanceof expressValidation.ValidationError) {
    res.status(err.status).json(err);
  } else {
    res.status(500)
    .json({
      status: err.status,
      message: err.message
    });
  }
});
app.use('/api', routes);

export default app;
