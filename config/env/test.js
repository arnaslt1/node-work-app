/**
 * Created by arnasmizara on 06/10/2018.
 */
export default {
  env: 'test',
  db: 'mongodb://localhost/node-work-app-test',
  port: 3000,
};