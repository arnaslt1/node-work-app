import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import { expect } from 'chai';
import sinon from 'sinon';
import app from '../../../index';
import { clearDatabase } from '../../helpers/ClearDB';
import Car from '../../models/car';
import Buyer from '../../models/buyer';
import Model from '../../models/model';

import Promise from 'Bluebird'

require('sinon-mongoose');
require('sinon-as-promised');


describe('Purchase API Tests', () => {
  let sandbox, model, car, buyer;

  before((done) => {
    Promise.try(() => {
     return  Model.create({
        model: 'BMW',
        make: "7 series"
      })
    }).then(m => {
      model = m
      return Car.create({
        VIN: '123TestVin',
        Model: m._id,
        Year: 2009
      })
    })
    .then(c => {
      car = c
      return Buyer.create({
        name: 'testName',
        surname: 'testSurname'
      })
    })
    .then(b => {
      buyer = b
      done();
    })
  })

    beforeEach((done) => {
      clearDatabase(() => {
        sandbox = sinon.sandbox.create();
        done();
      });
    });

    afterEach((done) => {
      sandbox.restore();
      done();
    });

    describe('GET /purchase', () => {
    });

    describe('GET /purchase/:purchaseId', () => {

    });

    describe('POST /purchase', () => {
      it('should return the created purchase successfully', (done) => {
        request(app)
        .post('/api/purchase')
        .send({
          Buyer: buyer._id,
          Car: car._id,
          Price: 500,
        })
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.Buyer).to.equal(buyer._id.toString());
          expect(res.body.Car).to.equal(car._id.toString());
          expect(res.body.Price).to.equal(500);
          done();
        });
      });
      it('should return Bad Request when missing buyer', (done) => {
        request(app)
        .post('/api/purchase')
        .send({
          Car: car._id,
          Price: 500,
        })
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done());
      });
      it('should return Bad Request when missing Car', (done) => {
        request(app)
        .post('/api/purchase')
        .send({
          Buyer: buyer._id,
          Price: 500,
        })
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done());
      });
    });

    describe('PUT /car/:carId', () => {

    });

    describe('DELETE /car/:carId', () => {

    });
  });