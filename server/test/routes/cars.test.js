import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import { expect } from 'chai';
import sinon from 'sinon';
import app from '../../../index';
import { clearDatabase } from '../../helpers/ClearDB';
import Car from '../../models/car';
import Model from '../../models/model';

require('sinon-mongoose');
require('sinon-as-promised');

describe('Cars API Tests', () => {
  let sandbox, model;

  before((done) => {
    Model.create({
      model: 'BMW',
      make: "7 series"
    }).then((m) => {
      model = m;
      done();
    })
  });

  beforeEach((done) => {
    clearDatabase(() => {
      sandbox = sinon.sandbox.create();
      done();
    });
  });

  afterEach((done) => {
    sandbox.restore();
    done();
  });

  describe('GET /car', () => {
  });

  describe('GET /car/:carId', () => {

  });

  describe('POST /car', () => {
    it('should return the created car successfully', (done) => {
      request(app)
      .post('/api/car')
      .send({
        VIN: '123TestVin',
        Model: model._id,
        Year: 2009
      })
      .expect(httpStatus.OK)
      .then(res => {
        expect(res.body.VIN).to.equal('123TestVin');
        expect(res.body.Model).to.equal(model._id.toString());
        expect(res.body.Year).to.equal(2009);
        done();
      });
    });
    it('should return Bad Request when missing model', (done) => {
      request(app)
      .post('/api/car')
      .send({
        VIN: '123TestVin',
        Year: 2009
      })
      .expect(httpStatus.BAD_REQUEST)
      .then(() => done());
    });
  });

  describe('PUT /car/:carId', () => {

  });

  describe('DELETE /car/:carId', () => {

  });
});