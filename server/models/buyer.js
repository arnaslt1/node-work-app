/**
 * Created by arnasmizara on 07/10/2018.
 */
import mongoose from 'mongoose';

const BuyerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  surname: {
    type: String,
    required: true,
    trim: true
  },
});

export default mongoose.model('Buyer', BuyerSchema);