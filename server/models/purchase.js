/**
 * Created by arnasmizara on 07/10/2018.
 */
import mongoose from 'mongoose';

const PurchaseSchema = new mongoose.Schema({
  Buyer: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'buyer'
  },
  Car: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'car'
  },
  Price: {
    type: Number,
    required: true,
    trim: true
  },
});

export default mongoose.model('Purchase', PurchaseSchema)