/**
 * Created by arnasmizara on 07/10/2018.
 */
import mongoose from 'mongoose';

const ModelSchema = new mongoose.Schema({
  model: {
    type: String,
    required: true,
    trim: true
  },
  make: {
    type: String,
    required: true,
    trim: true
  },
});

export default mongoose.model('Model', ModelSchema);