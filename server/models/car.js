import mongoose from 'mongoose';

const CarSchema = new mongoose.Schema({
  VIN: {
    type: String,
    required: true,
    trim: true
  },
  Model: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'model'
  },
  Year: {
    type: Number,
    required: true,
    trim: true
  },
});

export default mongoose.model('Car', CarSchema);