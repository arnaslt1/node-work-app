/**
 * Created by arnasmizara on 07/10/2018.
 */
import express from 'express';
import purchaseCtrl from '../controllers/purchase';
import validate from 'express-validation';
import validations from './validation/purchase';

const router = express.Router();

router.route('/')

.get(purchaseCtrl.list)

.post(validate(validations.createPurchase),
  purchaseCtrl.create);


router.route('/:purchaseId')

.get(purchaseCtrl.get)

.put(validate(validations.updatePurchase),

  purchaseCtrl.update)

.delete(purchaseCtrl.remove);


router.param('purchaseId', validate(validations.getPurchase));
router.param('purchaseId', purchaseCtrl.load);

export default router;
