/**
 * Created by arnasmizara on 06/10/2018.
 */
import Joi from 'joi';

export default {
  // POST /api/car
  createCar: {
    body: {
      VIN: Joi.string().required(),
      Model: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
      Year: Joi.number().integer().min(1900).max(2018).required(),
    }
  },

  // GET-PUT-DELETE /api/car/:carId
  getCar: {
    params: {
      carId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
    }
  },

  // PUT /api/car/:carId
  updateCar: {
    body: {
      VIN: Joi.string().required(),
      Model: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
      Year: Joi.number().integer().min(1900).max(2018).required(),
    }
  }
};