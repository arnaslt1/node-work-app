
/**
 * Created by arnasmizara on 06/10/2018.
 */
import Joi from 'joi';

export default {
  // POST /api/purchase
  createPurchase: {
    body: {
      Buyer: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
      Car: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
      Price: Joi.number().integer(),
    }
  },

  // GET-PUT-DELETE /api/purchase/:purchaseId
  getPurchase: {
    params: {
      carId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
    }
  },

  // PUT /api/purchase/:purchaseId
  updatePurchase: {
    body: {
      Buyer: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
      Car: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
      Price: Joi.number().integer(),

    }
  }
};