/**
 * Created by arnasmizara on 07/10/2018.
 */
import express from 'express';
import buyerCtrl from '../controllers/buyer';

const router = express.Router();
router.route('/')
.get(buyerCtrl.list)

.post(buyerCtrl.create);

router.route('/:buyerId')
.get(buyerCtrl.get)

.put(buyerCtrl.update)

.delete(buyerCtrl.remove);

router.param('buyerId', buyerCtrl.load);

export default router;
