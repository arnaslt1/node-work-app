/**
 * Created by arnasmizara on 06/10/2018.
 */
import express from 'express';
import carCtrl from '../controllers/car';
import validate from 'express-validation';
import validations from './validation/car';

const router = express.Router();

router.route('/')

.get(carCtrl.list)

.post(validate(validations.createCar),
  carCtrl.create);

router.route('/:carId')
.get(carCtrl.get)
.put(validate(validations.updateCar),
  carCtrl.update)
.delete(carCtrl.remove);

router.param('carId', validate(validations.getCar));
router.param('carId', carCtrl.load);

export default router;