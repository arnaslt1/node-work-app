/**
 * Created by arnasmizara on 07/10/2018.
 */
import express from 'express';
import modelCtrl from '../controllers/model';

const router = express.Router();
router.route('/')
.get(modelCtrl.list)

.post(modelCtrl.create);

router.route('/:modelId')
.get(modelCtrl.get)

.put(modelCtrl.update)

.delete(modelCtrl.remove);

router.param('modelId', modelCtrl.load);

export default router;
