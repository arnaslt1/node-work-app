import express from 'express';

const router = express.Router();

import carRoutes from './car';
import modelRoutes from './model';
import buyerRoutes from './buyer';
import purchaseRoutes from './purchase';


// GET to check api status
router.get('/api-status', (req, res) =>
  res.json({
    status: "ok"
  })
);

router.use('/car', carRoutes);
router.use('/model', modelRoutes);
router.use('/buyer', buyerRoutes);
router.use('/purchase', purchaseRoutes);


export default router;