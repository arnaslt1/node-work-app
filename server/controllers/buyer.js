/**
 * Created by arnasmizara on 07/10/2018.
 */
import Buyer from '../models/buyer';

function load(req, res, next, id) {
  Buyer.findById(id)
  .exec()
  .then((buyer) => {
    if (!buyer) {
      return res.status(404).json({
        status: 400,
        message: "Buyer not found"
      });
    }
    req.dbBuyer = buyer;
    return next();
  }, (e) => next(e));
}

function get(req, res) {
  return res.json(req.dbBuyer);
}

function create(req, res, next) {
  Buyer.create({
    name: req.body.name,
    surname: req.body.surname,
  })
  .then((savedBuyer) => {
    return res.json(savedBuyer);
  }, (e) => next(e));
}

function update(req, res, next) {
  const buyer = req.dbBuyer;
  Object.assign(buyer, req.body);

  buyer.save()
  .then(() => res.sendStatus(204),
    (e) => next(e));
}

function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Buyer.find()
  .skip(skip)
  .limit(limit)
  .exec()
  .then((buyers) => res.json(buyers),
    (e) => next(e));
}

function remove(req, res, next) {
  const buyer = req.dbBuyer;
  buyer.remove()
  .then(() => res.sendStatus(204),
    (e) => next(e));
}

export default { load, get, create, update, list, remove };