/**
 * Created by arnasmizara on 06/10/2018.
 */
import Purchase from '../models/purchase';

function load(req, res, next, id) {
  Purchase.findById(id)
  .exec()
  .then((purchase) => {
    req.dbPurchase = purchase;
    return next();
  }, (e) => next(e));
}

function get(req, res) {
  return res.json(req.dbPurchase);
}

function create(req, res, next) {
  Purchase.create({
    Buyer: req.body.Buyer,
    Car: req.body.Car,
    Price: req.body.Price
  })
  .then((savePurchase) => {
    return res.json(savePurchase);
  }, (e) => next(e));
}

function update(req, res, next) {
  const purchase = req.dbPurchase;
  Object.assign(purchase, req.body);

  car.save()
  .then(() => res.sendStatus(204),
    (e) => next(e));
}
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Purchase.find()
  .skip(skip)
  .limit(limit)
  .exec()
  .then((purchase) => res.json(purchase),
    (e) => next(e));
}

function remove(req, res, next) {
  const purchase = req.dbPurchase;
  purchase.remove()
  .then(() => res.sendStatus(204),
    (e) => next(e));
}

export default { load, get, create, update, list, remove };