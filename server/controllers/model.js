/**
 * Created by arnasmizara on 07/10/2018.
 */
import Model from '../models/model';

function load(req, res, next, id) {
  Model.findById(id)
  .exec()
  .then((model) => {
    if (!model) {
      return res.status(404).json({
        status: 400,
        message: "Model not found"
      });
    }
    req.dbModel = model;
    return next();
  }, (e) => next(e));
}

function get(req, res) {
  return res.json(req.dbModel);
}

function create(req, res, next) {
  Model.create({
    model: req.body.model,
    make: req.body.make,

  })
  .then((savedModel) => {
    return res.json(savedModel);
  }, (e) => next(e));
}

function update(req, res, next) {
  const model = req.dbModel;
  Object.assign(model, req.body);

  model.save()
  .then(() => res.sendStatus(204),
    (e) => next(e));
}

function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Model.find()
  .skip(skip)
  .limit(limit)
  .exec()
  .then((models) => res.json(models),
    (e) => next(e));
}

function remove(req, res, next) {
  const model = req.dbModel;
  model.remove()
  .then(() => res.sendStatus(204),
    (e) => next(e));
}

export default { load, get, create, update, list, remove };