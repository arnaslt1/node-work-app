/**
 * Created by arnasmizara on 06/10/2018.
 */
import Car from '../models/car';

function load(req, res, next, id) {
  Car.findById(id)
  .exec()
  .then((purchase) => {
    req.dbCar = purchase;
    return next();
  }, (e) => next(e));
}

function get(req, res) {
  return res.json(req.dbCar);
}

function create(req, res, next) {
  Car.create({
    VIN: req.body.VIN,
    Model: req.body.Model,
    Year: req.body.Year
  })
  .then((savedCar) => {
    return res.json(savedCar);
  }, (e) => next(e));
}

function update(req, res, next) {
  const car = req.dbCar;
  Object.assign(car, req.body);

  car.save()
  .then(() => res.sendStatus(204),
    (e) => next(e));
}
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Car.find()
  .skip(skip)
  .limit(limit)
  .exec()
  .then((cars) => res.json(cars),
    (e) => next(e));
}

function remove(req, res, next) {
  const car = req.dbCar;
  car.remove()
  .then(() => res.sendStatus(204),
    (e) => next(e));
}

export default { load, get, create, update, list, remove };