// API is deployed here https://dygfeo8iva.execute-api.eu-central-1.amazonaws.com/dev/

// /api is the proxy

// GET /api/api-status should return status ok if api is working

// FOR COMPLETE LOGIC LOOK AT ./server/routes

// THE SERVER IS BUILT ON EXPRESS AND MONGODB

// FOR VALIDATIONS I USE JOI SEE ./SERVER/ROUTES/VALIDATION

// FOR TESTS I USE MOCHA, SEE ./SERVER/ROUTES/TEST

// GULP MOCHA - LAUNCHES TESTS

// GULP NODEMON - IS FOR LOCAL DEVELOPMENT , SEE ./CONFIG/ENV/DEVELOPMENT

// TO DEPLOY npm run deploy


/*
      BUYERS
*/

// GET /api/buyer should return list of buyers

// GET /api/buyer/:buyerId returns buyer by id

// POST /api/buyer expects name and surname strings, creates a new buyer

// PUT /api/buyer/:buyerId  updates buyer

// DELETE api/buyer/:buyerId deletes buyer

/*
      MODELS
*/

// GET /api/model should return list of models

// GET /api/model/:modelId returns model by id

// POST /api/model expects model and make strings, creates a new model

// PUT /api/model/:modelId  updates model

// DELETE api/model/:modelId deletes model

/*
      Cars
*/

// GET /api/car should return list of cars

// GET /api/car/:carId return car by id

// POST /api/car expects VIN string, model Id of model object, year a number,  creates a new car

// PUT /api/car/:carId  updates car

// DELETE api/car/:carId deletes car

/*
      Purchases
*/

// GET /api/purchase should return list of purchases

// GET /api/purchase/:purchaseId return purchase by id

// POST /api/purchase expects Car object Id, Buyer object id, Price number, creates a new purchase

// PUT /api/purchase/:purchaseId  updates purchase

// DELETE api/purchase/:purchaseId deletes purchase